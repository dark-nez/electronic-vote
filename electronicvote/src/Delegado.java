
import clases.conectar;
import com.orsonpdf.PDFDocument;
import com.orsonpdf.PDFGraphics2D;
import com.orsonpdf.Page;
import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.Rectangle;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import org.jfree.*;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import com.lowagie.text.Document;
import com.lowagie.text.PageSize;
import com.lowagie.text.pdf.DefaultFontMapper;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfTemplate;
import com.lowagie.text.pdf.PdfWriter;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.Rectangle2D;
import java.io.FileOutputStream;



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author antpe
 */
public class Delegado extends javax.swing.JFrame {
    conectar con=new conectar();
    static String[] nameCand={"Voto en","","",""};
    static String[] firstnameCand={"Blanco","","",""};
    int year = Calendar.getInstance().get(Calendar.YEAR);
    
    
        /*PDFDocument pdfDoc = new PDFDocument();
        Page page = pdfDoc.createPage(new Rectangle(612, 468));
        PDFGraphics2D g2 = page.getGraphics2D();*/
        
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        JFreeChart chart=ChartFactory.createBarChart("Votación Personería Estudiantil COLSAM "+year, "", "", dataset, PlotOrientation.VERTICAL, false, false, false);

    /**
     * Creates new form Delegado
     */
    int[] votos={0,0,0,0};//vector que guardará la cantidad de votos
    
    public Delegado() {
        initComponents();
        setResizable(false);//no deja que se agrande o disminuya la ventana
        cargarCandidatos();
        cargarVotos();
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); //No hace nada al presionar el botón salir
        addWindowListener(new frameClose()); //Se agregan instrucciones al presionar el botón salir
        setTitle("Proceso de la votación"); 
        
    }
     
    @Override
    public Image getIconImage(){
        Image retValue = Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("images/icon.png"));
        return retValue;
    }
    
    
    void cargarCandidatos(){
        Connection cn= con.conexion();
        int cand=0;
       //Este código es para seleccionar la informacion de la tabla cands que es donde se encuentran todos los candidatos
        String sql="SELECT * FROM cands";
        try {
            Statement st=cn.createStatement();
            ResultSet rs=st.executeQuery(sql);
            while(rs.next()){
                nameCand[cand]=rs.getString("name");
                firstnameCand[cand]=rs.getString("firstname");
                cand++;
                
                }
            cn.close();
        } catch (SQLException ex) {
            Logger.getLogger(electronicvote.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    void cargarVotos(){
        Connection cn= con.conexion();
        //Consulta SQL de los votos por candidato        
       //Este código es para seleccionar la informacion de la tabla cands que es donde se encuentran todos los candidatos
        String sql="SELECT id_cand, COUNT(id_cand) AS vote FROM votes GROUP BY id_cand ORDER BY id_cand";
        try {
            Statement st=cn.createStatement();
            ResultSet rs=st.executeQuery(sql);
            while(rs.next()){
                votos[rs.getInt("id_cand")]=rs.getInt("vote");
                }
            cn.close();
        } catch (SQLException ex) {
            Logger.getLogger(electronicvote.class.getName()).log(Level.SEVERE, null, ex);
        }
        //
        
        dataset.setValue(votos[1], "", nameCand[1]+" "+firstnameCand[1]);
        dataset.setValue(votos[2], "", nameCand[2]+" "+firstnameCand[2]);
        dataset.setValue(votos[3], "", nameCand[3]+" "+firstnameCand[3]);
        dataset.setValue(votos[0], "", nameCand[0]+" "+firstnameCand[0]);
        
        
        
        
        
        ChartPanel chartPanel=new ChartPanel(chart);
        panel.add(chartPanel , BorderLayout.CENTER );
        
        
        

        //chart.draw(g2, new Rectangle(0, 0, 612, 468));

        
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        panel = new javax.swing.JPanel();
        jButton2 = new javax.swing.JButton();

        jButton1.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        jButton1.setText("Votar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setIconImage(getIconImage());
        setLocation(new java.awt.Point(350, 150));
        setMinimumSize(new java.awt.Dimension(820, 591));

        panel.setBackground(new java.awt.Color(153, 153, 153));
        panel.setLayout(new java.awt.BorderLayout());

        jButton2.setText("Guardar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(377, 377, 377)
                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(409, Short.MAX_VALUE))
            .addComponent(panel, javax.swing.GroupLayout.DEFAULT_SIZE, 887, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(panel, javax.swing.GroupLayout.PREFERRED_SIZE, 527, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(22, 22, 22)
                .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, 31, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

       
    }//GEN-LAST:event_jButton1ActionPerformed

    
    public static void writeChartToPDF(JFreeChart chart, int width, int height,
    String fileName) {
    PdfWriter writer = null;
    Document document = new Document(PageSize.A4.rotate());
    try {
     writer = PdfWriter.getInstance(document, new FileOutputStream(
       fileName));
     document.open();

     PdfContentByte contentByte = writer.getDirectContent();
     PdfTemplate template = contentByte.createTemplate(width, height);
        Graphics2D graphics2d = template.createGraphics(width, height,
       new DefaultFontMapper());
        Rectangle2D rectangle2d = new Rectangle2D.Double(0, 0, width,
       height);
     chart.draw(graphics2d, rectangle2d);
     graphics2d.dispose();
     contentByte.addTemplate(template, 0, 0);
    } catch (Exception e) {
     e.printStackTrace();
    }
    document.close();
 }
    
    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
    JFrame parentFrame = new JFrame();
 
    JFileChooser fileChooser = new JFileChooser();
    fileChooser.setDialogTitle("Especifica la ubicación y el nombre con el que deseas guardar el archivo.");   
 
    int userSelection = fileChooser.showSaveDialog(parentFrame);
 
    if (userSelection == JFileChooser.APPROVE_OPTION) {
    File fileToSave = fileChooser.getSelectedFile();
    
        //pdfDoc.setTitle("Votación Personería Estudiantil COLSAM "+year);
        //pdfDoc.setAuthor("Votaciones Colsam");
        
        writeChartToPDF(chart, 830, 550, fileToSave.getAbsolutePath()+".pdf");
        
        File myFile = new File(fileToSave.getAbsolutePath()+".pdf");
        //pdfDoc.writeToFile(myFile);
        
        
        
            if (Desktop.isDesktopSupported()) {
                    try {
                        Desktop.getDesktop().open(myFile);
                    } catch (IOException e) {
                        // System probably doesn't have a default PDF program
                    }
                }   
    }
    }//GEN-LAST:event_jButton2ActionPerformed

    
    // Frame Closer Listener Eschucha que se ha seleccionado el botón salir y se ejecutan las siguientes instrucciones
    public class frameClose extends WindowAdapter{ 
        @Override public void windowClosing(WindowEvent e){ 
            
            if(electronicvote.ad==0){
            Administrador.Habilitar();
            setVisible(false);//Cierra este form
            }else{
            setVisible(false);//Cierra este form
            electronicvote login=new electronicvote();        
            login.setVisible(true);//Abre el de login 
            }
        } 
    }
    /**
     * @param args the command line arguments
     */
        public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Delegado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Delegado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Delegado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Delegado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Delegado().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JPanel panel;
    // End of variables declaration//GEN-END:variables
}
