
import clases.conectar;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author antpe
 */
public class Administrador extends javax.swing.JFrame {
    conectar cc=new conectar();
    static JFrame jf = new Administrador();
    
    public Administrador() {
        
        
        initComponents();
        setResizable(false);//no deja que se agrande o disminuya la ventana
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); //No hace nada al presionar el botón salir
        addWindowListener(new frameClose()); //Se agregan instrucciones al presionar el botón salir
        setTitle("Administrador"); 
        
        btnPass.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));
        btnPass.setContentAreaFilled(false);
        btnPass.setFocusPainted(false);
        //setVerticalAlignment(SwingConstants.TOP);
        btnPass.setAlignmentY(Component.TOP_ALIGNMENT);
        btnPass.setToolTipText("Cambiar contraseña");
        
        btncand.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));
        btncand.setContentAreaFilled(false);
        btncand.setFocusPainted(false);
        //setVerticalAlignment(SwingConstants.TOP);
        btncand.setAlignmentY(Component.TOP_ALIGNMENT);
        btncand.setToolTipText("Gestionar candidatos");
        
        btnestad.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));
        btnestad.setContentAreaFilled(false);
        btnestad.setFocusPainted(false);
        //setVerticalAlignment(SwingConstants.TOP);
        btnestad.setAlignmentY(Component.TOP_ALIGNMENT);
        btnestad.setToolTipText("Ver proceso de votación");
        
        download.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));
        download.setContentAreaFilled(false);
        download.setFocusPainted(false);
        //setVerticalAlignment(SwingConstants.TOP);
        download.setAlignmentY(Component.TOP_ALIGNMENT);
        download.setToolTipText("Descargar plantilla de Excel"); 
        
        btnusers.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));
        btnusers.setContentAreaFilled(false);
        btnusers.setFocusPainted(false);
        //setVerticalAlignment(SwingConstants.TOP);
        btnusers.setAlignmentY(Component.TOP_ALIGNMENT);
        btnusers.setToolTipText("Gestionar ususarios"); 
        electronicvote.ad=0;
        
        Help.setBorder(BorderFactory.createEmptyBorder(9,9,9,9));
        Help.setContentAreaFilled(false);
        Help.setFocusPainted(false);
    }
 
    public static void Habilitar(){
        jf.setEnabled(true);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    
    // Frame Closer Listener Eschucha que se ha seleccionado el botón salir y se ejecutan las siguientes instrucciones
    public class frameClose extends WindowAdapter{ 
        @Override public void windowClosing(WindowEvent e){ 
            setVisible(false);//Cierra este form
            electronicvote login=new electronicvote();        
            login.setVisible(true);//Abre el de login
        } 
    }
    

    @Override
    public Image getIconImage(){
        Image retValue = Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("images/icon.png"));
        return retValue;
    }
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel10 = new javax.swing.JLabel();
        jMenu1 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();
        jMenu3 = new javax.swing.JMenu();
        jLabel3 = new javax.swing.JLabel();
        btnPass = new javax.swing.JButton();
        download = new javax.swing.JButton();
        btnestad = new javax.swing.JButton();
        btnusers = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        btncand = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        Help = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel10.setText("Gestionar usuarios");

        jMenu1.setText("jMenu1");

        jMenu2.setText("jMenu2");

        jMenu3.setText("jMenu3");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel3.setText("Gestionar contraseña");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setIconImage(getIconImage());
        setLocation(new java.awt.Point(350, 150));
        setMinimumSize(new java.awt.Dimension(820, 530));
        getContentPane().setLayout(null);

        btnPass.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/x128-lock.png"))); // NOI18N
        btnPass.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPassActionPerformed(evt);
            }
        });
        getContentPane().add(btnPass);
        btnPass.setBounds(210, 130, 130, 130);

        download.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/download-2.png"))); // NOI18N
        download.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                downloadActionPerformed(evt);
            }
        });
        getContentPane().add(download);
        download.setBounds(620, 390, 50, 50);

        btnestad.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/x128-statics.png"))); // NOI18N
        btnestad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnestadActionPerformed(evt);
            }
        });
        getContentPane().add(btnestad);
        btnestad.setBounds(210, 310, 130, 130);

        btnusers.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/x128-editar_doc.png"))); // NOI18N
        btnusers.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnusersActionPerformed(evt);
            }
        });
        getContentPane().add(btnusers);
        btnusers.setBounds(450, 310, 130, 130);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setText("Cambiar contraseña");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(210, 260, 170, 30);

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel7.setText("Revisar proceso de votación");
        getContentPane().add(jLabel7);
        jLabel7.setBounds(180, 430, 210, 50);

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel8.setText("Gestionar candidatos");
        getContentPane().add(jLabel8);
        jLabel8.setBounds(440, 260, 150, 30);

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel9.setText("Gestionar usuarios");
        getContentPane().add(jLabel9);
        jLabel9.setBounds(450, 440, 130, 30);

        btncand.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/x128-addUser.png"))); // NOI18N
        btncand.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncandActionPerformed(evt);
            }
        });
        getContentPane().add(btncand);
        btncand.setBounds(450, 130, 130, 130);

        jLabel1.setFont(new java.awt.Font("Bookman Old Style", 0, 36)); // NOI18N
        jLabel1.setText("<html><body><center>Bienvenido Administrador</body></html>");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(170, 10, 530, 106);

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel11.setText("Plantilla Excel");
        getContentPane().add(jLabel11);
        jLabel11.setBounds(600, 440, 100, 20);

        Help.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/search.png"))); // NOI18N
        Help.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                HelpActionPerformed(evt);
            }
        });
        getContentPane().add(Help);
        Help.setBounds(790, 0, 20, 25);

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/fondo admi.jpg"))); // NOI18N
        getContentPane().add(jLabel5);
        jLabel5.setBounds(0, 0, 830, 530);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnPassActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPassActionPerformed
        change_password login=new change_password();   
        login.setVisible(true);//Abre el de login
        this.setEnabled(false);
    }//GEN-LAST:event_btnPassActionPerformed

    private void btnestadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnestadActionPerformed
        Delegado login=new Delegado();      
        login.setVisible(true);//Abre el de login 
        this.setEnabled(false);
    }//GEN-LAST:event_btnestadActionPerformed

    private void downloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_downloadActionPerformed
        // TODO add your handling code here:
    JFrame parentFrame = new JFrame();
 
    JFileChooser fileChooser = new JFileChooser();
    fileChooser.setDialogTitle("Especifica la ubicación y el nombre con el que deseas guardar el archivo.");   
 
    int userSelection = fileChooser.showSaveDialog(parentFrame);
 
    if (userSelection == JFileChooser.APPROVE_OPTION) {
    File fileToSave = fileChooser.getSelectedFile();
    File fileTo = new File(fileToSave.getAbsolutePath()+".xlsx");
    File fileFrom = new File("src//images//plantilla.xlsx");
    
        try {
            //pdfDoc.setTitle("Votación Personería Estudiantil COLSAM "+year);
            //pdfDoc.setAuthor("Votaciones Colsam");

            Files.copy(fileFrom.toPath(), fileTo.toPath());
        } catch (IOException ex) {
            Logger.getLogger(Administrador.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        File myFile = new File(fileTo.getAbsolutePath());
        //pdfDoc.writeToFile(myFile);
        
        
        
            if (Desktop.isDesktopSupported()) {
                    try {
                        Desktop.getDesktop().open(myFile);
                    } catch (IOException e) {
                        // System probably doesn't have a default PDF program
                    }
                }   
    }
    }//GEN-LAST:event_downloadActionPerformed

    
    
    private void btnusersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnusersActionPerformed
        // TODO add your handling code here:
        int confirm= JOptionPane.showConfirmDialog(rootPane, "<html><body><br>Al momento de subir el archivo excel con los datos de los usuarios -estudiantes, administrador, delegado-<br> (que debe estar en el formato suministrado), se borrará la información de las votaciones actuales, por lo<br> tanto se le recomienda guardar el archivo PDF en la vista 'Revisar proceso de votación'.<br><br> <center>¿Desea continuar?</center></body></html>");
        if(confirm == JOptionPane.YES_OPTION){
        JFileChooser fc = new JFileChooser(); //Abrir Selector de Archivos
                FileNameExtensionFilter filter = new FileNameExtensionFilter("Archivos de Excel", "xls", "xlsx");//Filtro de tipo de archivos sólo exel
                fc.setFileFilter(filter);//asignar filtro al Selector de Archivos
                int status = fc.showOpenDialog(null);
                if (status == JFileChooser.APPROVE_OPTION) {//Si se hace clic en Aceptar
                File f = fc.getSelectedFile(); //Obtener archivo seleccionado
                String filename = f.getAbsolutePath();//devuelve la ruta absoluta absoluta asociada al objeto
                FileInputStream input;
            try {
                input = new FileInputStream(f);
                XSSFWorkbook wb = new XSSFWorkbook(input);  
                XSSFSheet sheet = wb.getSheetAt(0);  
                Row row;
                Connection cn=cc.conexion();
                String delete = "DELETE FROM users WHERE type_of_user= 'Estudiante';";
                PreparedStatement pst=cn.prepareStatement(delete);
                pst.executeUpdate();
                Statement st=cn.createStatement();
                String alter = "ALTER TABLE users AUTO_INCREMENT=3;";
                st.executeUpdate(alter);
                for(int i=1; i<=sheet.getLastRowNum(); i++){
                    row = sheet.getRow(i);
                    String name = row.getCell(0).getStringCellValue();
                    String firstname = row.getCell(1).getStringCellValue();
                    int grade = (int) row.getCell(2).getNumericCellValue();
                    String tdni = row.getCell(3).getStringCellValue();
                    int dni = (int) row.getCell(4).getNumericCellValue();
                    int password = (int) row.getCell(5).getNumericCellValue();
                    String type_of_user = row.getCell(6).getStringCellValue();

                    String sql = "INSERT INTO users (name, firstname, grade, tdni, dni, password, type_of_user) VALUES ('"+
                            name+"','"+firstname+"',"+grade+",'"+tdni+"',"+dni+","+password+",'"+type_of_user+"');";
                    pst=cn.prepareStatement(sql);
                    int n=pst.executeUpdate();
                
                    if(n<0){
                        JOptionPane.showMessageDialog(null, "Ha ocurrido un error, los datos de "+name+" "+firstname+" no se han añadido correctamente");
                    System.out.println("Import rows "+i);
                    }
                    
                }cn.close();
                JOptionPane.showMessageDialog(null, "Los datos se han añadido correctamente.");
            } catch (SQLException ex) {
            Logger.getLogger(Administrador.class.getName()).log(Level.SEVERE, null, ex);
            }   catch (FileNotFoundException ex) {
                Logger.getLogger(Administrador.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(Administrador.class.getName()).log(Level.SEVERE, null, ex);
            }
                
                
            }
        }else {
           JOptionPane.showMessageDialog(null, "Regrese al momento de guardar el PDF");
        }
    }//GEN-LAST:event_btnusersActionPerformed

    private void HelpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_HelpActionPerformed
        try{
            File help = new File("src//images//Manual.pdf");
            Desktop.getDesktop().open(help);
        } catch (IOException ex) {
            Logger.getLogger(electronicvote.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_HelpActionPerformed

    private void btncandActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncandActionPerformed
        add_cand login=new add_cand();      
       this.setEnabled(false); 
       login.setVisible(true);//Abre el de login 
        
    }//GEN-LAST:event_btncandActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Administrador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Administrador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Administrador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Administrador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Administrador().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Help;
    private javax.swing.JButton btnPass;
    private javax.swing.JButton btncand;
    private javax.swing.JButton btnestad;
    private javax.swing.JButton btnusers;
    private javax.swing.JButton download;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    // End of variables declaration//GEN-END:variables
}
