import clases.conectar;
import com.mysql.jdbc.PreparedStatement;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
/**
 *
 * @author antpe
 */
public class Estudiante extends javax.swing.JFrame {
    
    conectar cc=new conectar();
    static String[] nameCand={"Blanco","","",""};
    static String[] firstnameCand={"Blanco","","",""};
    String[] proposalsCand={"Blanco","","",""};
    String fullname = electronicvote.fullname; //Recibe el nombre completo desde el login
    int dniuser = electronicvote.dniuser; //Recibe el id del usuario desde el login
    
    
    public Estudiante() {
        initComponents();
        setTitle("Votación"); 
        
        cargarCandidatos();
        
        setResizable(false);//no deja que se agrande o disminuya la ventana
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); //No hace nada al presionar el botón salir
        addWindowListener(new frameClose()); //Se agregan instrucciones al presionar el botón salir
        
        jLabel7.setText(fullname);//Asigna al label el nombre completo del usuario
        
        cargarImagen();
        
        lblcand1.setVisible(false);
        lblcand2.setVisible(false);
        lblcand3.setVisible(false);
        lblBlanco.setVisible(false);
    }
    
    void cargarImagen(){
        File f = new File(".");//Ruta del Proyecto
        String path = f.getAbsolutePath(); //Obtener Ruta del Proyecto
        path = path.substring(0, path.length()-2)+"/src/images/candi_0.png";//Modificar ruta y añadir la de las imágenes
        ImageIcon icon0 = new ImageIcon(new ImageIcon(path).getImage().getScaledInstance(160, 220, Image.SCALE_DEFAULT));
        btnBlanco.setIcon(icon0);
        
        ImageIcon iconVote = new ImageIcon(new ImageIcon(getClass().getResource("/images/vote.png")).getImage().getScaledInstance(160, 220, Image.SCALE_DEFAULT));
        lblcand1.setIcon(iconVote);
        lblcand2.setIcon(iconVote);
        lblcand3.setIcon(iconVote);
        lblBlanco.setIcon(iconVote);
        path = f.getAbsolutePath(); //Obtener Ruta del Proyecto
        path = path.substring(0, path.length()-2)+"/src/images/candi_1.png";//Modificar ruta y añadir la de las imágenes
        ImageIcon icon1 = new ImageIcon(new ImageIcon(path).getImage().getScaledInstance(160, 220, Image.SCALE_DEFAULT));
        btncand1.setIcon(icon1);
        path = f.getAbsolutePath(); //Obtener Ruta del Proyecto
        path = path.substring(0, path.length()-2)+"/src/images/candi_2.png";//Modificar ruta y añadir la de las imágenes
        ImageIcon icon2 = new ImageIcon(new ImageIcon(path).getImage().getScaledInstance(160, 220, Image.SCALE_DEFAULT));
        btncand2.setIcon(icon2);
        path = f.getAbsolutePath(); //Obtener Ruta del Proyecto
        path = path.substring(0, path.length()-2)+"/src/images/candi_3.png";//Modificar ruta y añadir la de las imágenes
        ImageIcon icon3 = new ImageIcon(new ImageIcon(path).getImage().getScaledInstance(160, 220, Image.SCALE_DEFAULT));
        btncand3.setIcon(icon3);
    }
    
     @Override
    public Image getIconImage(){
        Image retValue = Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("images/icon.png"));
        return retValue;
    }
    
    void cargarCandidatos(){
        int cand=0;
       //Este código es para seleccionar la informacion de la tabla cands que es donde se encuentran todos los candidatos
        String sql="SELECT * FROM cands";
        try {
            Connection cn=cc.conexion();
            Statement st=cn.createStatement();
            ResultSet rs=st.executeQuery(sql);
            while(rs.next()){
                nameCand[cand]=rs.getString("name");
                firstnameCand[cand]=rs.getString("firstname");
                proposalsCand[cand]=rs.getString("proposals");
                cand++;
                
                }
            cn.close();
        } catch (SQLException ex) {
            Logger.getLogger(electronicvote.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        btncand1.setToolTipText("<html><strong>Numero del tarjeton:</strong>01<br><strong>Nombre: </strong>"+nameCand[1]+"<br><strong>Propuestas: </strong>"+proposalsCand[1]+"</html>");
        cand1.setText("<html><center>"+nameCand[1]+"<br>"+firstnameCand[1]+"</center></html");
        btncand2.setToolTipText("<html><strong>Numero del tarjeton:</strong>02<br><strong>Nombre: </strong>"+nameCand[2]+"<br><strong>Propuestas: </strong>"+proposalsCand[2]+"</html>");
        cand2.setText("<html><center>"+nameCand[2]+"<br>"+firstnameCand[2]+"</center></html");
        btncand3.setToolTipText("<html><strong>Numero del tarjeton:</strong>03<br><strong>Nombre: </strong>"+nameCand[3]+"<br><strong>Propuestas: </strong>"+proposalsCand[3]+"</html>");
        cand3.setText("<html><center>"+nameCand[3]+"<br>"+firstnameCand[3]+"</center></html");//toma cada informacion del candidato como el numero del tarjeton, nombre y propuestas
        Blanco.setText("<html><center>"+nameCand[0]+"<br>"+firstnameCand[0]+"</center></html");

    }
    void votar(){
        if(!lblcand1.isVisible() && !lblcand2.isVisible() && !lblcand3.isVisible() && !lblBlanco.isVisible() ){
            JOptionPane.showMessageDialog(null, fullname + ", debes seleccionar un candidato.");
        }else{
        try{
            Connection cn=cc.conexion();
            PreparedStatement pps = (PreparedStatement) cn.prepareStatement("INSERT INTO votes (dni_user, id_cand) VALUES(?,?)");
            pps.setInt(1, dniuser);
            pps.setInt(2, user_vote);
            pps.executeUpdate();
            JOptionPane.showMessageDialog(null, fullname +", Muchas gracias por su voto.");
            this.setVisible(false);
            electronicvote login=new electronicvote();        
            login.setVisible(true);//Abre el de login    
            cn.close();
        }   catch (Exception ex){
            String y = ex.toString();
            if(y.compareTo("com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException: Duplicate entry '123' for key 'dni_user'")==0){
                JOptionPane.showMessageDialog(null, fullname +", usted ya ha votado, debe salir.");
            }else{
            System.out.println(ex);
            }
            }
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        lblcand1 = new javax.swing.JLabel();
        btncand1 = new javax.swing.JButton();
        lblcand2 = new javax.swing.JLabel();
        btncand2 = new javax.swing.JButton();
        lblcand3 = new javax.swing.JLabel();
        btncand3 = new javax.swing.JButton();
        lblBlanco = new javax.swing.JLabel();
        btnBlanco = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        cand1 = new javax.swing.JLabel();
        cand3 = new javax.swing.JLabel();
        Blanco = new javax.swing.JLabel();
        cand2 = new javax.swing.JLabel();
        btnVotar = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setIconImage(getIconImage());
        setLocation(new java.awt.Point(350, 150));
        setMinimumSize(new java.awt.Dimension(820, 536));
        getContentPane().setLayout(null);

        jLabel1.setFont(new java.awt.Font("Bookman Old Style", 0, 36)); // NOI18N
        jLabel1.setText("<html><body><center>Selecciona el candidato por el cual deseas votar</body></html>");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(130, 50, 530, 106);

        lblcand1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/vote.png"))); // NOI18N
        getContentPane().add(lblcand1);
        lblcand1.setBounds(30, 210, 150, 180);

        btncand1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/candi_1.png"))); // NOI18N
        btncand1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(51, 51, 51), new java.awt.Color(153, 153, 153), new java.awt.Color(153, 153, 153), null));
        btncand1.setOpaque(false);
        btncand1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncand1ActionPerformed(evt);
            }
        });
        getContentPane().add(btncand1);
        btncand1.setBounds(30, 210, 150, 180);

        lblcand2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/vote.png"))); // NOI18N
        getContentPane().add(lblcand2);
        lblcand2.setBounds(200, 210, 150, 180);

        btncand2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/candi_2.png"))); // NOI18N
        btncand2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(51, 51, 51), new java.awt.Color(153, 153, 153), new java.awt.Color(153, 153, 153), null));
        btncand2.setOpaque(false);
        btncand2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncand2ActionPerformed(evt);
            }
        });
        getContentPane().add(btncand2);
        btncand2.setBounds(200, 210, 150, 180);

        lblcand3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/vote.png"))); // NOI18N
        lblcand3.setMaximumSize(new java.awt.Dimension(137, 172));
        lblcand3.setMinimumSize(new java.awt.Dimension(137, 172));
        lblcand3.setPreferredSize(new java.awt.Dimension(137, 172));
        getContentPane().add(lblcand3);
        lblcand3.setBounds(380, 210, 150, 180);

        btncand3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/candi_3.png"))); // NOI18N
        btncand3.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(51, 51, 51), new java.awt.Color(153, 153, 153), new java.awt.Color(153, 153, 153), null));
        btncand3.setOpaque(false);
        btncand3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncand3ActionPerformed(evt);
            }
        });
        getContentPane().add(btncand3);
        btncand3.setBounds(380, 210, 150, 180);

        lblBlanco.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/vote.png"))); // NOI18N
        getContentPane().add(lblBlanco);
        lblBlanco.setBounds(580, 210, 150, 180);

        btnBlanco.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/candi_0.png"))); // NOI18N
        btnBlanco.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(51, 51, 51), new java.awt.Color(153, 153, 153), new java.awt.Color(153, 153, 153), null));
        btnBlanco.setOpaque(false);
        btnBlanco.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBlancoActionPerformed(evt);
            }
        });
        getContentPane().add(btnBlanco);
        btnBlanco.setBounds(580, 210, 150, 180);

        jLabel7.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        jLabel7.setText("Nombre del Estudiante");
        getContentPane().add(jLabel7);
        jLabel7.setBounds(20, 20, 630, 30);

        cand1.setBackground(new java.awt.Color(0, 153, 204));
        cand1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        cand1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        cand1.setText("Candidato 1");
        cand1.setOpaque(true);
        getContentPane().add(cand1);
        cand1.setBounds(30, 400, 150, 30);

        cand3.setBackground(new java.awt.Color(0, 153, 204));
        cand3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        cand3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        cand3.setText("Candidato 3");
        cand3.setOpaque(true);
        getContentPane().add(cand3);
        cand3.setBounds(380, 400, 150, 30);

        Blanco.setBackground(new java.awt.Color(0, 153, 204));
        Blanco.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        Blanco.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Blanco.setText("Candidato 0");
        Blanco.setOpaque(true);
        getContentPane().add(Blanco);
        Blanco.setBounds(580, 400, 150, 30);

        cand2.setBackground(new java.awt.Color(0, 153, 204));
        cand2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        cand2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        cand2.setText("Candidato 2");
        cand2.setOpaque(true);
        getContentPane().add(cand2);
        cand2.setBounds(200, 400, 150, 30);

        btnVotar.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        btnVotar.setText("Votar");
        btnVotar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVotarActionPerformed(evt);
            }
        });
        getContentPane().add(btnVotar);
        btnVotar.setBounds(320, 460, 100, 30);

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/fondo.jpg"))); // NOI18N
        getContentPane().add(jLabel5);
        jLabel5.setBounds(0, 0, 820, 530);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    

    
    // Frame Closer Listener Eschucha que se ha seleccionado el botón salir y se ejecutan las siguientes instrucciones
    public class frameClose extends WindowAdapter{ 
        @Override public void windowClosing(WindowEvent e){ 
            dispose();//Cierra este form
            electronicvote login=new electronicvote();        
            login.setVisible(true);//Abre el de login
        } 
    }
        
    
    
    private void btncand1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncand1ActionPerformed
        // TODO add your handling code here:
        user_vote= 1;
        lblcand1.setVisible(true);
        lblcand2.setVisible(false);
        lblcand3.setVisible(false);
        lblBlanco.setVisible(false);
    }//GEN-LAST:event_btncand1ActionPerformed

    private void btncand2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncand2ActionPerformed
       // TODO add your handling code here:
        user_vote= 2;
       lblcand1.setVisible(false);
        lblcand2.setVisible(true);
        lblcand3.setVisible(false);
        lblBlanco.setVisible(false);
    }//GEN-LAST:event_btncand2ActionPerformed

    private void btncand3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncand3ActionPerformed
        // TODO add your handling code here:
        user_vote= 3;
        lblcand1.setVisible(false);
        lblcand2.setVisible(false);
        lblcand3.setVisible(true);
        lblBlanco.setVisible(false);
    }//GEN-LAST:event_btncand3ActionPerformed

    private void btnBlancoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBlancoActionPerformed
        // TODO add your handling code here:
        user_vote= 0;
        lblcand1.setVisible(false);
        lblcand2.setVisible(false);
        lblcand3.setVisible(false);
        lblBlanco.setVisible(true);
    }//GEN-LAST:event_btnBlancoActionPerformed

    private void btnVotarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVotarActionPerformed
        votar();
    }//GEN-LAST:event_btnVotarActionPerformed
    
    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Estudiante.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Estudiante.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Estudiante.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Estudiante.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Estudiante().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Blanco;
    private javax.swing.JButton btnBlanco;
    private javax.swing.JButton btnVotar;
    private javax.swing.JButton btncand1;
    private javax.swing.JButton btncand2;
    private javax.swing.JButton btncand3;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JLabel cand1;
    private javax.swing.JLabel cand2;
    private javax.swing.JLabel cand3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel lblBlanco;
    private javax.swing.JLabel lblcand1;
    private javax.swing.JLabel lblcand2;
    private javax.swing.JLabel lblcand3;
    // End of variables declaration//GEN-END:variables
private int user_vote;
}
